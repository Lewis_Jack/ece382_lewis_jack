// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/


#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"

/**************LEFTOVERS FROM LAB 17************************/

/**************Initial values used for all programs******************/
volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

uint8_t LineData;   // direct measure from line sensor
int32_t Position;   // position in 0.1mm relative to center of line

#define PWMNOMINAL 5000
/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}

#define TACHBUFF 10                      // number of elements in tachometer array
int32_t ActualSpeedL, ActualSpeedR;      // Actual speed
int32_t ErrorL, ErrorR;                  // X* - X'
int32_t PrevErrorL;
int32_t PrevErrorR;
uint16_t LeftTach[TACHBUFF];             // tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;              // direction of left rotation (FORWARD, STOPPED, REVERSE)
int32_t LeftSteps;                       // number of tachometer steps of left wheel (units of 220/360 = 0.61 mm traveled)
uint16_t RightTach[TACHBUFF];            // tachometer period of right wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection RightDir;             // direction of right rotation (FORWARD, STOPPED, REVERSE)
int32_t RightSteps;                      // number of tachometer steps of right wheel (units of 220/360 = 0.61 mm traveled)

int i = 0;

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i = 0;
  uint32_t sum = 0;
  for(i=0; i<length; i++){
    sum = sum + array[i];
  }
  return (sum/length);
}

#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230


volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosError;
int32_t Error;

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_14_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
uint8_t KpSys = 3;
#define PWMNOMINALFIN 3000
#define SWINGFIN 1000
#define PWMINFIN (PWMNOMINALFIN-SWINGFIN)
#define PWMAXFIN (PWMNOMINALFIN+SWINGFIN)

void SysTick_Handler(void){
    if(Mode){
        // Determine set point
        SetPoint = (Left + Right)/2;

        // set error based off set point
        Error = SetPoint - Right;

        // update duty cycle based on proportional control
        UR = PWMNOMINAL + KpSys*Error;
        UL = PWMNOMINAL - KpSys*Error;

        // check to ensure not too big of a swing
        if(UR>PWMAXFIN){
            UR = PWMAXFIN;
        }
        else if(UR<PWMINFIN){
            UR = PWMINFIN;
        }
        if(UL>PWMAXFIN){
            UL = PWMAXFIN;
        }
        else if(UL<PWMINFIN){
            UL = PWMINFIN;
        }
        // update motor values
        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
}


//////////////////////////
/// FINAL PROJECT CODE ///
//////////////////////////

//LEVEL 2//

#define TARGETDEG 90
#define WHEELRAD 35
#define ROBOTRAD 60

//uint8_t iFlag;
//uint8_t tFlag;
//
//uint32_t turnError;
//
//uint32_t leftStepHit;
//uint32_t leftStepGoal;
uint32_t stopTime;

uint32_t Input;
int32_t InitSteps;


uint32_t KpFin = 8;

enum State {
    Forward,
    Stop,
    BackTurn,
    Turn
};

enum State funct;


////void doBack(void){
////    Time++;
////    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
////    leftStepHit = RightSteps;
////    leftStepGoal = DEG90 * 2;
////    if(Time<400){
////        Motor_Backward(UL,UR);
////    }
////    Time = 0;
////}

/*
* Proportional controller to drive robot
* using line following
*/
void doForward(void){
    // read values from line sensor, similar to
    // SysTick_Handler() in Lab10_Debugmain.c
    Time++;
    if(Time%10 == 0){
        Reflectance_Start();
    }
    // Use Reflectance_Position() to find position
    if(Time%10 == 1){
        LineData = Reflectance_End();
        Position = Reflectance_Position(LineData);

        if(Mode){
        // update duty cycle based on porportional control
        UR = PWMNOMINALFIN - KpFin*Position;
        UL = PWMNOMINALFIN + KpFin*Position;

        //check swing, same as in SysTick but with version 3 instead of 2
        if(UR>PWMAXFIN){
           UR = PWMAXFIN;
        }
        if(UL>PWMAXFIN){
           UL = PWMAXFIN;
        }
        if(UR<PWMINFIN){
           UR = PWMINFIN;
        }
        if(UL<PWMINFIN){
           UL = PWMINFIN;
        }

        Motor_Forward(UL, UR);
        ControllerFlag = 1;
    }
    }
}


int32_t Deg=0;
void doBackTurn(void){
//  doBack();

    Time++; //increment time
    if(Time<500){ //back up for a sec
        Motor_Backward(PWMNOMINALFIN, PWMNOMINALFIN);
    }
    //else if(Time==500){ //once back up a lil then
        //InitialSteps = LeftSteps; //initialize steps
    //}
    else{ //start turning tf around
        Motor_Right(PWMNOMINALFIN, PWMNOMINALFIN); //turn right 180 degrees
        Deg = (abs(LeftSteps - InitSteps)*35)/60;
        if(abs(Deg) >= 90*3){
            //Input = 1; //move to next state to go back to starting line
            funct = Forward;
        }
    }
}

void doTurn(void){
    Input = 0;
    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
    Motor_Right(PWMNOMINALFIN, PWMNOMINALFIN); //turn right 180 degrees
    Deg = (abs(LeftSteps - InitSteps)*WHEELRAD)/ROBOTRAD;
    if(abs(Deg) >= TARGETDEG*3){
        //Input = 1; //move to next state to go back to starting line
        funct = Forward;
    }
}

void doStopFlash(void){
        Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);
        Motor_Stop();

        if (stopTime < 250){
            LaunchPad_Output(1); //red
        }
        else if (stopTime < 400){
            LaunchPad_Output(4); //blue
        }
        else{
            stopTime = 0;
        }

        stopTime++;

}
//FSM STRUCTURE//
//struct State{
//    char name[9];
//    void (*funct1)(void);
//    const struct State *next[4];
//};typedef const struct State State_t;
//
//
//#define Forward &Level1[0]
//#define Turn    &Level1[1]
//#define BackTurn &Level1[2]
//#define StopFlash &Level1[3]
//
//State_t Level1[4] = {
//{"Forward  ", &doForward, {Forward, Turn, BackTurn, StopFlash}},
//{"Turn ",     &doTurn, {Turn, Forward, Forward, Forward}},
//{"BackTurn ", &doBackTurn, {BackTurn, Forward, Forward, Forward}},
//{"StopFlash", &doStopFlash, {StopFlash, StopFlash, StopFlash, StopFlash}},
//
//};
//
//State_t *Spt;

// next state transition
void ControllerFin(){
    Time++;
    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);

    //FSM CONTROLLER//
//    Spt->funct1();
//    Spt = Spt->next[Input];
//
//    if(LineData == 0xF0){
//        InitSteps = LeftSteps;
//        Input = 0;
//        Spt = Turn; //left turn
//    }
//    if(LineData == 0xDB || LineData == 0x6D || LineData == 0xB6){
//        Spt = StopFlash; //stop
//    }

    //Enumerated Controller//
    switch(funct){
    case Forward:
        doForward();
        break;
    case BackTurn:
        doBackTurn();
        break;
    case Turn:
        doTurn();
        break;
    case Stop:
        doStopFlash();
        break;
    }
}

int main(void){
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Reflectance_Init();
    Motor_Init();
    Tachometer_Init();

    TimerA1_Init(&ControllerFin, 500); // utilize fsm for level 1

    Motor_Stop();
    Mode = 0;
    Time = 0;
    UR = UL = PWMNOMINALFIN;
    EnableInterrupts();

    //Input = 0;
    //Spt = Forward;

    funct = Forward;

    Pause3();

    while(1){
        if(Bump_Read()){
            Time = 0;
            InitSteps = LeftSteps; //initialize steps
            //Input = 0;
            //Spt = BackTurn;
            funct = BackTurn;
        }
        else{
            funct = Forward;
        }
    }
}

