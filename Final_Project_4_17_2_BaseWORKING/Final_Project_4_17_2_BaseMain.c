// Lab17_Control.c
// Runs on MSP432
// Implementation of the control system.
// Daniel and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/


#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/PWM.h"
#include "../inc/LaunchPad.h"
#include "../inc/UART0.h"
#include "../inc/Motor.h"
#include "../inc/Bump.h"
#include "../inc/ADC14.h"
#include "../inc/TimerA1.h"
#include "../inc/IRDistance.h"
#include "../inc/Nokia5110.h"
#include "../inc/LPF.h"
#include "../inc/SysTickInts.h"
#include "../inc/Tachometer.h"
#include "../inc/Reflectance.h"
#include "../inc/Classifier.h"

/**************Initial values used for all programs******************/


volatile uint32_t ControllerFlag; // set every 10ms on controller execution

int32_t Mode = 0;
int32_t UL, UR;             // Controller output PWM duty 2 to 14,998
uint32_t Time; // in 0.01 sec

#define PWMNOMINAL 5000
/**************Functions used by all programs***********************/
void Pause3(void){
    int j;
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(200); LaunchPad_Output(0); // off
    Clock_Delay1ms(200); LaunchPad_Output(1); // red
  }
  while(Bump_Read()==0){// wait for touch
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(3); // red/green
  }
  while(Bump_Read()){ // wait for release
    Clock_Delay1ms(100); LaunchPad_Output(0); // off
    Clock_Delay1ms(100); LaunchPad_Output(4); // blue
  }
  for(j=1000;j>100;j=j-200){
    Clock_Delay1ms(j); LaunchPad_Output(0); // off
    Clock_Delay1ms(j); LaunchPad_Output(2); // green
  }
  // restart Jacki
  UR = UL = PWMNOMINAL;    // reset parameters
  Mode = 1;
  ControllerFlag = 0;
  Time = 0;
}


int i = 0;

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i = 0;
  uint32_t sum = 0;
  for(i=0; i<length; i++){
    sum = sum + array[i];
  }
  return (sum/length);
}

/**************Program17_2******************************************/
// Proportional controller to drive between two walls using IR sensors
// distances in mm
#define TOOCLOSE 110
#define DESIRED_DIST 172
#define TOOFAR 230
#define PWMNOMINAL2 9500
#define SWING2 3200
#define PWMIN2 (PWMNOMINAL2-SWING2)
#define PWMAX2 (PWMNOMINAL2+SWING2)

volatile uint32_t nr, nc, nl; // raw distance values
int32_t Left, Center, Right; // IR distances in mm
volatile uint32_t ADCflag; // Set every 500us on ADC sample
int32_t DataBuffer[5];
int32_t SetPoint = 172;
uint32_t PosIRerror;
int32_t IRerror;
scenario_t turn;


// proportional controller gain 

void IRsampling(void){
    uint32_t raw17, raw12, raw16;
    ADC_In17_14_16(&raw17, &raw12, &raw16);
    nr = LPF_Calc(raw17);
    nc = LPF_Calc2(raw12);
    nl = LPF_Calc3(raw16);
    Left = LeftConvert(nl);
    Center = CenterConvert(nc);
    Right = RightConvert(nr);
    ADCflag = 1;
}

/*
* Proportional controller to keep robot in
* center of two walls using IR sensors.
*/
int32_t Kp2= 22;

    void SysTick_Handler(void){
        //Add-Ins//
        turn = Classify(Left, Center, Right);
        //IMAGINARY WALLS//
        if(turn == LeftJoint || turn == TeeJoint){
            Left = DESIRED_DIST;
        }
        if(Mode){
            // Determine set point
            SetPoint = (Left + Right)/2;

            // set IRerror based off set point
            IRerror = SetPoint - Right;

            // update duty cycle based on proportional control
            UR = PWMNOMINAL2 + Kp2*IRerror;
            UL = PWMNOMINAL2 - Kp2*IRerror;

            // check to ensure not too big of a swing
            if(UR>PWMAX2){
                UR = PWMAX2;
            }
            else if(UR<PWMIN2){
                UR = PWMIN2;
            }
            if(UL>PWMAX2){
                UL = PWMAX2;
            }
            else if(UL<PWMIN2){
                UL = PWMIN2;
            }
            // update motor values
            Motor_Forward(UL, UR);
            ControllerFlag = 1;
        }
    }



int main(void){
    uint32_t raw17,raw14,raw16;
    DisableInterrupts();
    Clock_Init48MHz();
    LaunchPad_Init();
    Bump_Init();
    Motor_Init();

    // user TimerA1 to sample the IR sensors at 2000 Hz
    TimerA1_Init(&IRsampling, 250);

    Motor_Stop();
    Mode = 0;
    UR = UL = PWMNOMINAL2;
    ADCflag = ControllerFlag = 0;   // semaphores

    ADC0_InitSWTriggerCh17_14_16();   // initialize channels 17,12,16
    ADC_In17_14_16(&raw17,&raw14,&raw16);  // sample
    LPF_Init(raw17,64);     // P9.0/channel 17
    LPF_Init2(raw14,64);    // P4.1/channel 12
    LPF_Init3(raw16,64);    // P9.1/channel 16

    // user SysTick to run the controller at ADJUSTED 125 Hz with a priority of 2
    SysTick_Init(4000,2);

    Pause3();

    EnableInterrupts();
    while(1){
        if(Bump_Read()){ // collision
            Mode = 0;
            Motor_Stop();
            Pause3();
        }
        if(ControllerFlag){ // 100 Hz, not real time
           ControllerFlag = 0;
        }
    }
}

