; Cryptography.asm
; Runs on any Cortex M
; Student name: C2C Jack Lewis
; Date: 18 August 2020
; Documentation statement: Attended EI with C2C Winding from Capt. Beyer where we were able
;						   to get the code working to a level before the final devlierable.
;						   Received help from Capt. Beyer when I could not find the last
;						   letters of the secret message - looping format error.
; Cryptography function to encrypt and decrypt a message using a 1 byte key.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.
       .thumb
       .data
       .align 2
encrypted_message .space 32 ;space designated for storing messages in memory
decrypted_message .space 32

       .text
       .align 2
Message1    .string "This is Message 1!#"
Message2    .string "This is Message 2!#"
Message3    .string "This is Message 3!#"
Key1        .byte 0xab
Key2        .byte 0xcd
Key3        .byte 0xef
SecretKey	.byte 0x42 ; "42" - the asnwer to life, universe, everything

EMessage    .word 0x71070107, 0x2B62707A ; encrypted message overheard between Capt Beyer and Dr. York
		    .word 0x2A366231, 0x27206227
		    .word 0x21623631, 0x3131232E
		    .word 0x00006163

       .global Encrypt
       .global Decrypt
       .global main

main:  .asmfunc
;	Encryption Function Commented Out to Test Decrypt
;	Load Registers to Encrypt
;	LDR R0,Message3pt              ;where decrypted message is
;	LDR R1,Key3pt				   ;key to encrypt with
;	LDR R2,encrypted_messagept     ;where encrypted message is stored

;	Encrypt Function
;   BL Encrypt


;	Load Registers to Decrypt
	LDR R0,EMessagept				;where encrypted message is
	LDR R1,SecretKeypt				;key to encrypted message
	LDR R2,decrypted_messagept		;where decrypted message is stored

;	Decrypt Function
	BL Decrypt

quit B quit                         ;infinite loop, to stall code at finish
    .endasmfunc





;Message Memory Pointers
encrypted_messagept   .word encrypted_message
decrypted_messagept   .word decrypted_message

;Message Value Pointers
Message1pt 		      .word Message1
Message2pt            .word Message2
Message3pt            .word Message3

;Key Pointers
Key1pt	              .word Key1
Key2pt                .word Key2
Key3pt                .word Key3

;Beyer York Encrypted Message
SecretKeypt 		  .word SecretKey
EMessagept			  .word EMessage

    .end
