; Encryption.asm
; Runs on any Cortex M
; Student name: C2C Jack Lewis
; Date: 21 Aug 2020
; Basic XOR encryption and decryption functions for cryptography.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.


       .thumb
       .text
       .align 2
       .global Encrypt
       .global Decrypt
       .global XOR_bytes

;------------Encrypt------------
; Takes in the location of a plain text message and key and
;	xors the message to encrypt it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input: R0 is message address, R1 is key address,
;	R2 is location to store encrypted message
; Output: encrypted message stored at memory
;	located at R2
; Modifies: R0, R1
Encrypt:   .asmfunc

	PUSH {R4-R5,LR}
	LDR R1, [R1]
	MOV R4,R0


	AND R1, R1, #0xFF  ; ANDs key to save only key and clears else

loop LDRB R0, [R4], #1 ;loads contents of r4 into r0 one byte at a time
	MOV R5,R0          ;copy R0 into R5

	BL XOR_bytes
	STRB R0, [R2], #1  ;stores contents of r0 into r2 one byte at a time

	CMP R5, #0x23      ;compares message before encryption with #
	BNE loop		   ;loop until cmp returns 1

	POP {R4-R5,PC}
		.endasmfunc

;------------Decrypt------------
; Takes in the location of an encrypted message and key and
;	xors the message to decrypt it it. It stores the
;	result at a passed in memory location. Stops
;	when the ASCII character # is encountered.
; Input: R0 is message address, R1 is key address,
;	R2 is location to store decrypted message
; Output: decrypted message stored at memory
;	located ate R2
; Modifies: R0, R1
Decrypt:	.asmfunc

	PUSH {R4-R5,LR}
	LDR R1, [R1]
	MOV R4,R0

	AND R1, R1, #0xFF   ; ANDs key to save only key and clear else

loop2 LDRB R0, [R4], #1 ; loads contents of r4 into r0 one byte at a time
	MOV R5,R0           ; copies r0 into r5

	BL XOR_bytes
	STRB R0, [R2], #1   ; stores contents of r0 into r2 one byte at a time

	CMP R0, #0x23       ; compares decrypted byte against #
	BNE loop2			; loop until cmp returns 1

	POP {R4-R5,PC}
	.endasmfunc

;------------XOR_Bytes------------
; Takes in two bytes, XORs them, returns the result
; Input: R0 message byte, R1 is key byte,
; Output: R0 is XOR result
; Modifies: R0
XOR_bytes:	.asmfunc

	EOR R0, R0, R1 ; XOR R0 and R1 and save in R0
	BX LR		   ; branches back to LR address
		.endasmfunc
		.align 4
    .end
