; Convert.asm
; Runs on any Cortex M
; Student name: C2C Jack Lewis
; Date: 24 August 2020
; Doc Statement: None outside of class. In class, verbally bounced ideas and syntax
;					back and forth with C2C Winding, deBastos, and Blois without
;					looking at examples of each others code.
; Conversion function for a GP2Y0A21YK0F IR sensor
; Jonathan Valvano
; July 11, 2019

; This example accompanies the book
;   "Embedded Systems: Introduction to Robotics,
;   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
; For more information about my classes, my research, and my books, see
; http://users.ece.utexas.edu/~valvano/
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2019, Jonathan Valvano, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.



       .thumb
       .text
       .align 2
       .global Convert

;------------Convert------------
; Calculate the distance in mm given the 14-bit ADC value
; D = 1195172/(n � 1058)
; Input: R0 is n, ADC value, 14-bit unsigned number 2552 to 16383
; Output: R0 is distance in mm
; If the ADC input is less than 2552, return 800 mm
; Modifies: R1, R2, R3
Convert:   .asmfunc

;		LDR R0, R0 ;loads r4 with n INITIALLY THOUGHT TO BE NEEDED BUT REMOVED AFTER DEBUGGING

		;Load Registers w Constants
		LDR R1, IRMax
		LDR R2, IROffset
		LDR R3, IRSlope

		ADD R2, R0, R2 ;adds the bottom term of division and stores in R2

		CMP R0, R1     ;compares R0 to R1 by subtracting R0 - R1
		BLT less	   ;if negative flag set, R0 less than threshold, loops to less
		B gteq	   ;if compare is equal, loops to gteq
			   ;if compare R0 greater than R1 also loops to gteq
less
		MOV R0, #800   ;loads 800mm into R0 to meet req's
		B done		   ;forces to loop to done and exit preventing change in R0

gteq
		SDIV R0, R3, R2	;performs signed division with R3 over R2 and stores into R0
		B done			;forces to loop to done and exit preventing change in R0
done
		BX LR			;branches immediate to link register and exits function Convert

      .endasmfunc
      .align 4
IRSlope  .word 1195172
IROffset .word -1058
IRMax    .word 2552

    .end
