// Lab14_EdgeInterruptsmain.c
// Runs on MSP432, interrupt version
// Main test program for interrupt driven bump switches the robot.
// Daniel Valvano and Jonathan Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// Negative logic bump sensors
// P4.7 Bump5, left side of robot
// P4.6 Bump4
// P4.5 Bump3
// P4.3 Bump2
// P4.2 Bump1
// P4.0 Bump0, right side of robot

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/LaunchPad.h"
#include "../inc/Motor.h"
#include "../inc/BumpInt.h"
#include "../inc/TExaS.h"
#include "../inc/TimerA1.h"
#include "../inc/FlashProgram.h"


#define REDLED (*((volatile uint8_t *)(0x42098060)))
#define BLUELED (*((volatile uint8_t *)(0x42098068)))

uint8_t CollisionData, CollisionFlag;
uint32_t Step;	// 0, 1, 2...,NUM-1
uint32_t Time;	// in ms
void HandleCollision(uint8_t bumpSensor){
	REDLED ^= 0x01;
	REDLED ^= 0x01;
	Time = Time + 1;
	REDLED ^= 0x01;
	Motor_Stop();
	CollisionData = bumpSensor;
	CollisionFlag = 1;
}
int Program14_1(void){  // test of interrupt-driven bump interface
	Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
	CollisionFlag = 0;
	Motor_Init();        // activate Lab 13 software
	LaunchPad_Init();
	Motor_Forward(7500,7500); // 50%
	BumpInt_Init(&HandleCollision);
	TExaS_Init(LOGICANALYZER_P4_765320);
	EnableInterrupts();
	while(1){
		BLUELED ^= 0x01;
	WaitForInterrupt();
	}
}

/****************Lab14******************/

// Struct to send a command to the robot
typedef struct command{
    uint16_t RightPWM; // 0 to 14998
    uint16_t LeftPWM; // 0 to 14998
    void (*MotorFunction)(uint16_t, uint16_t);
    uint32_t Duration; // time to run in ms
}command_t;

#define NUM 15
/* 
* Write this as part of Lab 14
* 	Code three high-level commands
*	1. Back up slowly for 1 second
*	2. Turn right slowly for 5 seconds (90 degrees)
*	3. Go forward quickly for 1 minute
*/
const command_t Control[NUM]={
//{	  2000,	  2000,	  &Motor_Backward,	  1000}, //simple command
//{   2000,   2000,   &Motor_Right,       5000}, //simple command
//{   7500,   7500,   &Motor_Forward,    60000}, //simple command
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Right,      280},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Right,      280},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Right,      280},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Right,      480},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Left,       230},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Left,       230},
{   8000,   7500,   &Motor_Forward,    2000},
{   8000,   7500,   &Motor_Left,       230},
{   8000,   7500,   &Motor_Forward,    20000},
};


/*
* Write this as part of Lab 14
* Controller to update command after current command is
* complete (duration)
*/
void Controller(void){
	Time++;
	if(Time > Control[Step].Duration){
	    Time=0;
	    Step++;
	    Step = Step % NUM;
	    Control[Step].MotorFunction(Control[Step].RightPWM, Control[Step].LeftPWM);

	}

}

/*
* Write this as part of Lab 14.
*	On a collision, stop the robot, restart the commands, 
*	run the backup command, and reset time
*/
void Collision(uint8_t bumpSensor){
	Motor_Stop();
	CollisionData = bumpSensor;
	CollisionFlag = 1;
	Time = 0;
	Step = 0;
	TimerA1_Stop();
}


int Program14_2(void){
	DisableInterrupts();
	Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
	LaunchPad_Init();
	Motor_Init();

	// Initialize Bump with the Collision() function you wrote
	BumpInt_Init(&Collision);
	// Initialize Timer A1 with the Controller you wrote at 1000 Hz
	TimerA1_Init(&Controller,500);
	// Initialize Step to the first command
	Step = 0;
	// Run first command
	Control[Step].MotorFunction(Control[Step].RightPWM, Control[Step].LeftPWM);
	
	// Initialize Time
	Time = 0;
	
	EnableInterrupts();
	while(1){
		WaitForInterrupt();
	}
}

int main(void){
//	Program14_1();
	Program14_2();
}
