// Lab16_Tachmain.c
// Runs on MSP432
// Test the operation of the tachometer by implementing
// a simple DC motor speed controller.
// Daniel Valvano
// July 11, 2019

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// See Bump.c for bumper connections (Port 8 or Port 4)

// Debug heartbeat connected to P2.0 (built-in red LED)
// Debug heartbeat connected to P2.4

// Pololu kit v1.1 connections:
// Left Encoder A connected to P10.5 (J5)
// Left Encoder B connected to P5.2 (J2.12)
// Right Encoder A connected to P10.4 (J5)
// Right Encoder B connected to P5.0 (J2.13)

// Left motor direction connected to P5.4 (J3.29)
// Left motor PWM connected to P2.7/TA0CCP4 (J4.40)
// Left motor enable connected to P3.7 (J4.31)
// Right motor direction connected to P5.5 (J3.30)
// Right motor PWM connected to P2.6/TA0CCP3 (J4.39)
// Right motor enable connected to P3.6 (J2.11)

// Negative logic bump sensors defined in Bump.c (use Port 4)
// P4.7 Bump5, left side of robot
// P4.6 Bump4
// P4.5 Bump3
// P4.3 Bump2
// P4.2 Bump1
// P4.0 Bump0, right side of robot

// Debug heartbeat connected to P2.0 (built-in red LED)
// Debug heartbeat connected to P1.0 (built-in LED1)

#include <stdint.h>
#include "msp.h"
#include "../inc/Clock.h"
#include "../inc/CortexM.h"
#include "../inc/LaunchPad.h"
#include "../inc/Motor.h"
#include "../inc/Nokia5110.h"
#include "../inc/Tachometer.h"
#include "../inc/TimerA1.h"
#include "../inc/TA3InputCapture.h"
#include "../inc/TExaS.h"
#include "../inc/FlashProgram.h"
#include "../inc/Bump.h"

#define P2_4 (*((volatile uint8_t *)(0x42098070)))
#define P2_3 (*((volatile uint8_t *)(0x4209806C)))
#define P2_2 (*((volatile uint8_t *)(0x42098068)))
#define P2_1 (*((volatile uint8_t *)(0x42098064)))
#define P2_0 (*((volatile uint8_t *)(0x42098060)))
#define P1_0 (*((volatile uint8_t *)(0x42098040)))

/**************Functions/Variables for Program16_1***************/

uint16_t Period0;              // (1/SMCLK) units = 83.3 ns units
uint16_t First0;               // Timer A3 first edge, P10.4
int Done0;                     // set each rising
// max period is (2^16-1)*83.3 ns = 5.4612 ms
// min period determined by time to run ISR, which is about 1 us
void PeriodMeasure0(uint16_t time){
  P2_0 = P2_0^0x01;            // thread profile, P2.0
  Period0 = (time - First0)&0xFFFF; // 16 bits, 83.3 ns resolution
  First0 = time;               // setup for next
  Done0 = 1;
}
uint16_t Period1;              // (1/SMCLK) units = 83.3 ns units
uint16_t First1;               // Timer A3 first edge, P10.5
int Done1;                     // set each rising
// max period is (2^16-1)*83.3 ns = 5.4612 ms
// min period determined by time to run ISR, which is about 1 us
void PeriodMeasure1(uint16_t time){
  P1_0 = P1_0^0x01;            // thread profile, P1.0
  Period1 = (time - First1)&0xFFFF; // 16 bits, 83.3 ns resolution
  First1 = time;               // setup for next
  Done1 = 1;
}

uint16_t SpeedBuffer0[500];      // RPM
uint16_t SpeedBuffer1[500];      // RPM
uint32_t PeriodBuffer0[500];     // 1/12MHz = 0.083 usec
uint32_t PeriodBuffer1[500];     // 1/12MHz = 0.083 usec
uint32_t Duty,DutyBuffer[500];  // 0 to 15000
uint32_t Time; // in 0.01 sec
void Collect(void){
  P2_1 = P2_1^0x01;    // thread profile, P2.1
  if(Done0==0) Period0 = 65534; // stopped
  if(Done1==0) Period1 = 65534; // stopped
  Done0 = Done1 = 0;   // set on subsequent
  if(Time==100){       // 2 sec
    Duty = 7500;
    Motor_Forward(Duty,Duty);    // 50%
  }
  if(Time==200){       // 4 sec
    Duty = 11250;
    Motor_Forward(Duty,Duty);  // 75%
  }
  if(Time==300){       // 6 sec
      Duty = 14999;
      Motor_Forward(Duty,Duty);  // 100%
    }
  if(Time==400){       // 10 sec
    Duty = 3750;
    Motor_Forward(Duty,Duty);    // 25%
  }
  if(Time<500){       // 10 sec
    SpeedBuffer0[Time] = 2000000/Period0;
    SpeedBuffer1[Time] = 2000000/Period1;
    PeriodBuffer0[Time] = Period0;
    PeriodBuffer1[Time] = Period1;
    DutyBuffer[Time] = Duty;
    Time = Time+1;
  }
  if((Time==500)||Bump_Read()){
    Duty = 0;
    Motor_Stop();     // 0%
    TimerA1_Stop();
  }
}

int Program16_1(void){
  DisableInterrupts();
  Clock_Init48MHz();   // 48 MHz clock; 12 MHz Timer A clock
  P2->SEL0 &= ~0x11;
  P2->SEL1 &= ~0x11;   // configure P2.0 and P2.4 as GPIO
  P2->DIR |= 0x11;     // P2.0 and P2.4 outputs
  First0 = First1 = 0; // first will be wrong
  Done0 = Done1 = 0;   // set on subsequent
  Time = 0; Duty = 3750;
  Bump_Init();
  Motor_Init();        // activate Lab 13 software
  TimerA3Capture_Init(&PeriodMeasure0,&PeriodMeasure1);
  TimerA1_Init(&Collect,5000); // 100 Hz
  Motor_Forward(3750,3750); // 25%
  TExaS_Init(LOGICANALYZER_P10);
  EnableInterrupts();
  while(1){
    WaitForInterrupt();
  }
}

/**************Functions/Variables for Program16_2***************/

#define DESIREDMAX 120
#define DESIREDMIN 30

#define MAXDUTY 14898
#define MINDUTY 100

// variables for controlling motors
int16_t DesiredL = 50;					// desired RPM left, set initially to 50
int16_t DesiredR = 50;					// desired RPM right, set initially to 50
uint16_t ActualL;                       // actual rotations per minute
uint16_t ActualR;                       // actual rotations per minute
uint16_t LeftDuty = 3750;               // duty cycle of left wheel (0 to 14,998)
uint16_t RightDuty = 3750;              // duty cycle of right wheel (0 to 14,998)

// variables to average tachometer readings 
#define TACHBUFF 10
uint16_t LeftTach[TACHBUFF];			// tachometer period of left wheel (number of 0.0833 usec cycles to rotate 1/360 of a wheel rotation)
enum TachDirection LeftDir;
int32_t LeftSteps;
uint16_t RightTach[TACHBUFF];
enum TachDirection RightDir;
int32_t RightSteps;

void LCDClear(void){
	Nokia5110_Init();
	Nokia5110_Clear();
	Nokia5110_OutString("Desired(RPM)L     R     Actual (RPM)L     R     Distance(mm)");
}

void LCDDesired(void){
	Nokia5110_SetCursor(1, 1);         // one leading space, second row
	Nokia5110_OutUDec(DesiredL);
	Nokia5110_SetCursor(7, 1);         // seven leading spaces, second row
	Nokia5110_OutUDec(DesiredR);
}

void LCDActual(void){
	Nokia5110_SetCursor(1, 3);       // one leading space, fourth row
	Nokia5110_OutUDec(ActualL);
	Nokia5110_SetCursor(7, 3);       // seven leading spaces, fourth row
	Nokia5110_OutUDec(ActualR);
	Nokia5110_SetCursor(0, 5);       // zero leading spaces, sixth row
	if(LeftSteps < 0){
		Nokia5110_OutChar('-');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		Nokia5110_OutUDec((-1*LeftSteps*220)/360); //negative left distance
	}else{
		Nokia5110_OutChar(' ');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		Nokia5110_OutUDec((LeftSteps*220)/360); //positive left distance
	}
	Nokia5110_SetCursor(6, 5);       // six leading spaces, sixth row
	if(RightSteps < 0){
		Nokia5110_OutChar('-');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		Nokia5110_OutUDec((-1*RightSteps*220)/360); //negative right distance
	}else{
		Nokia5110_OutChar(' ');
		// 70mm diameter wheel; ~220mm circumference divided by 360 steps
		Nokia5110_OutUDec((RightSteps*220)/360); //positive right distance
	}
}

// ------------avg------------
// Simple math function that returns the average
// value of an array.
// Input: array is an array of 16-bit unsigned numbers
//        length is the number of elements in 'array'
// Output: the average value of the array
// Note: overflow is not considered
uint16_t avg(uint16_t *array, int length){
  int i;
  uint32_t sum = 0;

  for(i=0; i<length; i++){
      sum += array[i];
  }

  return (sum/length);
}

void Program16_2(void){
	// write a main program that uses the tachometer
	int i = 0;
	Clock_Init48MHz();
	LCDClear();
	LaunchPad_Init();
	Bump_Init();
	Tachometer_Init();
	Motor_Init();
	EnableInterrupts();
	while(1){
		Motor_Stop();
		while(Bump_Read() == 0){
			// update the screen
			LCDDesired();
			
			if((LaunchPad_Input()&0x01)){
			// Button1 has been pressed - increase desired right speed by 10 and rollover if max is reached
			DesiredR += 10;
			if(DesiredR>DESIREDMAX){
			    DesiredR = DESIREDMIN;
			}
			
			
			}
			
			if((LaunchPad_Input()&0x02)){
			// Button2 has been pressed - increase desired left speed by 10 and rollover if max is reached
			DesiredL += 10;
			if(DesiredL>DESIREDMAX){
			    DesiredL = DESIREDMIN;
			}



			}
			
			// flash the blue LED
			i = i + 1;
			LaunchPad_Output((i&0x01)<<2);
			Clock_Delay1ms(200);               // delay ~0.2 sec at 48 MHz
		}

		for(i=0; i<10; i=i+1){
			// flash the yellow LED
			LaunchPad_Output(0x03);
			Clock_Delay1ms(100);               // delay ~0.1 sec at 48 MHz
			LaunchPad_Output(0x00);
			Clock_Delay1ms(100);               // delay ~0.1 sec at 48 MHz
		}

		LaunchPad_Output(0x02);
		i = 0;

		while(Bump_Read() == 0){
			// get values from the tachometer and store into variables
			// replace this line with function call to Tachometer_Get()
			
		    Tachometer_Get(&LeftTach[i], &LeftDir, &LeftSteps, &RightTach[i], &RightDir, &RightSteps);

			i = i + 1;
			if(i >= TACHBUFF){
				i = 0;
				// write code to get actual left and right values
				// omega = 2,000,000/n, where n is the number of timer pulses
				// Use the ave function to reduce the noise.
				ActualL = avg(LeftTach, TACHBUFF);
				ActualR = avg(RightTach, TACHBUFF);
				
				ActualL = 2000000/ActualL;
				ActualR = 2000000/ActualR;
				

				// very simple, very stupid controller
				// if actual is greater than (desired + 3) and leftduty is 100 more than min
				// subtract 100 from left duty
				if(ActualL>(DesiredL+3) && (LeftDuty - 100) > MINDUTY){
				    LeftDuty -= 100;
				}
				// if actual is less than (desired - 3) and leftduty is 100 less than max
				// add 100 to left duty
				if(ActualL<(DesiredL+3) && (LeftDuty + 100) < MAXDUTY){
				    LeftDuty += 100;
				}
				// repeat for right
                if(ActualR>(DesiredR+3) && (RightDuty - 100) > MINDUTY){
                    RightDuty -= 100;
                }
                if(ActualR<(DesiredR+3) && (RightDuty + 100) < MAXDUTY){
                    RightDuty += 100;
                }
				// drive motors forward
                Motor_Forward(RightDuty,LeftDuty);

				// update the screen
				LCDActual();
			}
			
			Clock_Delay1ms(100);			// delay ~0.1 sec at 48 MHz
		}
		Motor_Stop();
		i = 0;
		while(Bump_Read()){
			i = i + 1;
			LaunchPad_Output(i&0x01);
			Clock_Delay1ms(100);			// delay ~0.1 sec at 48 MHz
		}
	}
}

void main(void){
//    Program16_1();
    Program16_2();
}
