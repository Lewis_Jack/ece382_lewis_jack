################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432e401y.cmd 

ASM_SRCS += \
../Test.asm 

C_SRCS += \
../startup_msp432e401y_ccs.c \
../system_msp432e401y.c 

C_DEPS += \
./startup_msp432e401y_ccs.d \
./system_msp432e401y.d 

OBJS += \
./Test.obj \
./startup_msp432e401y_ccs.obj \
./system_msp432e401y.obj 

ASM_DEPS += \
./Test.d 

OBJS__QUOTED += \
"Test.obj" \
"startup_msp432e401y_ccs.obj" \
"system_msp432e401y.obj" 

C_DEPS__QUOTED += \
"startup_msp432e401y_ccs.d" \
"system_msp432e401y.d" 

ASM_DEPS__QUOTED += \
"Test.d" 

ASM_SRCS__QUOTED += \
"../Test.asm" 

C_SRCS__QUOTED += \
"../startup_msp432e401y_ccs.c" \
"../system_msp432e401y.c" 


