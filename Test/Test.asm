; Cryptography.asm
; Runs on any Cortex M
; Student name: C2C Jack Lewis
; Date: 18 August 2020
; Documentation statement: Attended EI with C2C Winding from Capt. Beyer where we were able
;						   to get the code working to a level before the final devlierable.
;						   Received help from Capt. Beyer when I could not find the last
;						   letters of the secret message - looping format error.
; Cryptography function to encrypt and decrypt a message using a 1 byte key.
; Capt Steven Beyer
; July 23 2020
;
;Simplified BSD License (FreeBSD License)
;Copyright (c) 2020, Steven Beyer, All rights reserved.
;
;Redistribution and use in source and binary forms, with or without modification,
;are permitted provided that the following conditions are met:
;
;1. Redistributions of source code must retain the above copyright notice,
;   this list of conditions and the following disclaimer.
;2. Redistributions in binary form must reproduce the above copyright notice,
;   this list of conditions and the following disclaimer in the documentation
;   and/or other materials provided with the distribution.
;
;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
;AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
;IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
;LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
;LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
;AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
;OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
;USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;The views and conclusions contained in the software and documentation are
;those of the authors and should not be interpreted as representing official
;policies, either expressed or implied, of the FreeBSD Project.
       .thumb
       .data
       .align 2
left .space 32 ;space designated for storing messages in memory
result .space 32

       .text
       .align 2
SIDEMIN  .byte 212
BLOCKED  .byte 1
STRAIGHT .byte 2


       .global main

main:  .asmfunc

	LDR R0, SIDEMIN
	LDR R1, BLOCKED
	LDR R2, STRAIGHT

quit B quit                         ;infinite loop, to stall code at finish
    .endasmfunc
    .end
