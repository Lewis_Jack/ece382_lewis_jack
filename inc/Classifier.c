/*
 * Classifier.c
 * Runs on MSP432
 *
 * Student name: C2C Jack Lewis
 * Date: 31 Aug 2020
 * Documentation: I completed all logical statements on my own accord. C2C Winding looked over my code and
 *                noticed that the way I stated my logic was flawed as I had attempted to simplify too many
 *                expressions and that (Right && Left) < SIDEMAX is not the same as (Right < SIDEMAX) && (Left < SIDEMAX)
 *
 * Conversion function for a GP2Y0A21YK0F IR sensor and classification function
 *  to take 3 distance readings and determine what the current state of the robot is.
 *
 *  Created on: Jul 24, 2020
 *  Author: Captain Steven Beyer
 *
 * This example accompanies the book
 * "Embedded Systems: Introduction to Robotics,
 * Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 * For more information about my classes, my research, and my books, see
 * http://users.ece.utexas.edu/~valvano/
 *
 * Simplified BSD License (FreeBSD License
 *
 * Copyright (c) 2019, Jonathan Valvano, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of the FreeBSD Project.
 */

#include <stdint.h>
#include "../inc/Classifier.h"

#define IRSlope 1195172
#define IROffset -1058
#define IRMax 2552
#define MaxDist 800


/* Convert
* Calculate the distance in mm given the 14-bit ADC value
* D = 1195172/(n - 1058)
*
* The maximum measurement distance for the sensor is 800 mm,
* so if the ADC value is less than 2552 (IRMAX), your
* function should return 800.
*
* Input
*   int32_t n:  14-bit ADC data
* Output
*   int32_t     Distance in mm
*/
int32_t Convert(int32_t n){

    int32_t result = IRSlope/(n + IROffset); //initialize result and perform calculation AS IF greater than IRMax

    if(n<IRMax){
        result = MaxDist; //if n passed in is < IRMAX sets equal to MaxDist
    }

    return result; //returns distance in mm
}



#define SIDEMAX 354    // largest side distance to wall in mm
#define SIDEMIN 212    // smallest side distance to wall in mm
#define CENTEROPEN 600 // distance to wall between open/blocked
#define CENTERMIN 150  // min distance to wall in the front
#define IRMIN 50       // min possible reading of IR sensor
#define IRMAX 800      // max possible reading of IR sensor

/* Classify
* Utilize three distance values from the left, center, and right
* distance sensors to determine and classify the situation into one
* of many scenarios (enumerated by scenario)
*
* Input
*   int32_t Left: distance measured by left sensor
*   int32_t Center: distance measured by center sensor
*   int32_t Right: distance measured by right sensor
*
* Output
*   scenario_t: value representing the scenario the robot
*       currently detects (e.g. RightTurn, LeftTurn, etc.)
*/
scenario_t Classify(int32_t Left, int32_t Center, int32_t Right){
    scenario_t result = Error;

    if((Left < IRMIN) || (Right < IRMIN) || (Center < IRMIN) || (Left > IRMAX) || (Right > IRMAX) || (Center > IRMAX)){
        result = Error; //returns error if right/center/left are greater than max or less than min values

    }
    else{ //if not out of initial range calculates errors first
        if(Center < CENTERMIN){
            result += CenterTooClose;
                }
        if(Right < SIDEMIN){
            result += RightTooClose;
                }
        if(Left < SIDEMIN){
            result += LeftTooClose;
                }
        //errors not initial will add and return unique numbers if more than one condition is present


        if((Left >= SIDEMIN) && (Right >= SIDEMIN) && (Center >= CENTERMIN)){ //if conditions met, searches for correct result below

            if(Center >= CENTEROPEN){                               //all conditions below have Center >= CENTEROPEN
                if((Left < SIDEMAX) && (Right < SIDEMAX)){
                    result = Straight;
                  }
                else if((Left >= SIDEMAX) && (Right < SIDEMAX)){
                    result = LeftJoint;
                  }
                else if((Right >= SIDEMAX) && (Left < SIDEMAX)){
                    result = RightJoint;
                  }
                else{
                    result = CrossRoad;
                  }
          }

            else{                                                   //all conditions below have Center < CENTEROPEN
                if((Left >= SIDEMAX) && (Right < SIDEMAX)){
                    result = LeftTurn;
                  }
                else if((Right >= SIDEMAX) && (Left < SIDEMAX)){
                    result = RightTurn;
                  }
                else if((Right >= SIDEMAX) && (Left >= SIDEMAX)){
                    result = TeeJoint;
                  }
                else{
                    result = Blocked;
                  }
          }
        }
    }
    return result; //returns result as enumerated type declared in .h file
}

