// Reflectance.c
// Provide functions to take measurements using the kit's built-in
// QTRX reflectance sensor array.  Pololu part number 3672. This works by outputting to the
// sensor, waiting, then reading the digital value of each of the
// eight phototransistors.  The more reflective the target surface is,
// the faster the voltage decays.
// Daniel and Jonathan Valvano
// July 11, 2019

//Edited by C2C Jack Lewis 14 Sep 2020
//Documentation: C2C Reynolds helped show me that I am completely inept and
//                cannot initialize all my inputs.

/* This example accompanies the book
   "Embedded Systems: Introduction to Robotics,
   Jonathan W. Valvano, ISBN: 9781074544300, copyright (c) 2019
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2019, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// reflectance even LED illuminate connected to P5.3
// reflectance odd LED illuminate connected to P9.2
// reflectance sensor 1 connected to P7.0 (robot's right, robot off road to left)
// reflectance sensor 2 connected to P7.1
// reflectance sensor 3 connected to P7.2
// reflectance sensor 4 connected to P7.3 center
// reflectance sensor 5 connected to P7.4 center
// reflectance sensor 6 connected to P7.5
// reflectance sensor 7 connected to P7.6
// reflectance sensor 8 connected to P7.7 (robot's left, robot off road to right)

#include <stdint.h>
#include "msp432.h"
#include "..\inc\Clock.h"

// ------------Reflectance_Init------------
// Initialize the GPIO pins associated with the QTR-8RC
// reflectance sensor.  Infrared illumination LEDs are
// initially off.
// Initialize even LED Illuminate connected to P5.3
// Initialize odd LED Illuminate connected to P9.2
// Initialize reflectance sensors connected to P7.0-P7.7
// Input: none
// Output: none
void Reflectance_Init(void){
    P5 -> SEL0 &= ~0x08;
    P5 -> SEL1 &= ~0x08;
    P5 -> DIR |= 0x08;
    P5 -> OUT = (P5 -> OUT & (~0x08)); //initializes P5.3 OUT
    P9 -> SEL0 &= ~0x04;
    P9 -> SEL1 &= ~0x04;
    P9 -> DIR |= 0x04;
    P9 -> OUT = (P9 -> OUT & (~0x04)); //initializes P9.2 OUT
    P7 -> SEL0 &= ~0xFF;
    P7 -> SEL1 &= ~0xFF;
    P7 -> DIR &= ~0xFF;                //initializes P7.0-7.7 IN
}

// ------------Reflectance_Read------------
// Read the eight sensors
// Turn on Even and Odd IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// wait time us
// Read sensors
// Turn off Even and Odd IR LEDs
// Input: time to wait in usec
// Output: sensor readings
// Assumes: Reflectance_Init() has been called
uint8_t Reflectance_Read(uint32_t time){
    uint8_t result;
    P5 -> OUT = (P5 -> OUT & (~0x08)) | 0x08;
    P9 -> OUT = (P9 -> OUT & (~0x04)) | 0x04;
    P7 -> DIR |= 0xFF;
    P7 -> OUT = ((P7 -> OUT) & (~0xFF)) | 0xFF;

    Clock_Delay1us(10); //pulse 8 sensors high for 10us
    P7 -> SEL0 &= ~0xFF;
    P7 -> SEL1 &= ~0xFF;
    P7 -> DIR &= ~0xFF; //pins become input

    Clock_Delay1us(time);

    result = (P7 -> IN);

    P5 -> OUT &= ~0x08;
    P9 -> OUT &= ~0x04; //turns off P5.3 and P9.2
  return result;
}

// ------------Reflectance_Center------------
// Read the two center sensors
// Turn on the 8 IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// wait t us
// Read sensors
// Turn off the 8 IR LEDs
// Input: time to wait in usec
// Output: 0 (off road), 1 off to left, 2 off to right, 3 on road
// (Left,Right) Sensors
// 1,1          both sensors   on line
// 0,1          just right     off to left
// 1,0          just left      off to right
// 0,0          neither        lost
// Assumes: Reflectance_Init() has been called

uint8_t Reflectance_Center(uint32_t time){
    uint8_t data = Reflectance_Read(time); //sets result equal to the result of R_Read
    data &= 0x18;                          //masks to grab 7.3 and 7.4 as center input
    data = data >> 3;                             //shifts 7.3 and 7.4 to MSB
  return data;
}


const int32_t Weight[8] = {334, 238, 142, 48, -48, -142, -238, -334};       // 0.1 mm
const int32_t Mask[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
// Perform sensor integration
// Input: data is 8-bit result from line sensor
// Output: position in 0.1mm relative to center of line
int32_t Reflectance_Position(uint8_t data){
    int32_t sum = 0;
    int32_t average = 0;
    int32_t count = 0;
    int i = 0;
    if(data){

       for(i = 0; i < 8; i++){ //loop through array of sensors

        if(data & Mask[i]){
           sum += Weight[i];       //add weights of sensor together
           count += 1;             //keeps track of the number of sensors "on"
        }
        else
           sum += 0;               //if sensor is not detected then it does not sum or count
           count += 0;

    }

}
    average = sum/count;
    return average;                //returns the average of values from "on" sensors
}

// ------------Reflectance_Start------------
// Begin the process of reading the eight sensors
// Turn on the 8 IR LEDs
// Pulse the 8 sensors high for 10 us
// Make the sensor pins input
// Input: none
// Output: none
// Assumes: Reflectance_Init() has been called
void Reflectance_Start(void){
    P5 -> OUT |= 0x08;
    P9 -> OUT |= 0x04; //turn on 8 IR LEDs

    P7 -> DIR  |= 0xFF; //pins become output
    P7 -> OUT  |= 0xFF; //set P7 high

    Clock_Delay1us(10);

    P7 -> DIR  &= ~0xFF; //pins become input
}


// ------------Reflectance_End------------
// Finish reading the eight sensors
// Read sensors
// Turn off the 8 IR LEDs
// Input: none
// Output: sensor readings
// Assumes: Reflectance_Init() has been called
// Assumes: Reflectance_Start() was called 1 ms ago
uint8_t Reflectance_End(void){

    uint8_t data = (P7->IN);
    P5 -> OUT &= ~0x08; //P5.3 off
    P9 -> OUT &= ~0x04; //P9.2 off

    return ~data; // return 8-bit sensor result
}

