// TA3InputCapture.c
// Runs on MSP432
// Use Timer A3 in capture mode to request interrupts on rising
// edges of P10.4 (TA3CCP0) and P8.2 (TA3CCP2) and call user
// functions.
// Daniel Valvano
// May 30, 2017

//Edited By: C2C Jack Lewis
// 10 Nov 2020
// Documentation: My actual duty values were fluctuating and C2C Winding and C2C Blois
//                  informed me to change my CCTL Value to 0x00000024 vs 0x00000224

/* This example accompanies the books
   "Embedded Systems: Introduction to the MSP432 Microcontroller",
       ISBN: 978-1512185676, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Interfacing to the MSP432 Microcontroller",
       ISBN: 978-1514676585, Jonathan Valvano, copyright (c) 2017
   "Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers",
       ISBN: 978-1466468863, , Jonathan Valvano, copyright (c) 2017
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/

Simplified BSD License (FreeBSD License)
Copyright (c) 2017, Jonathan Valvano, All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are
those of the authors and should not be interpreted as representing official
policies, either expressed or implied, of the FreeBSD Project.
*/

// external signal connected to P10.5 (TA3CCP1) (trigger on rising edge)
// external signal connected to P10.4 (TA3CCP0) (trigger on rising edge)

#include <stdint.h>
#include "msp.h"

void ta3dummy(uint16_t t){};       // dummy function
void (*CaptureTask0)(uint16_t time) = ta3dummy;// user function
void (*CaptureTask2)(uint16_t time) = ta3dummy;// user function

//------------TimerA3Capture_Init01------------
// Initialize Timer A3 in edge time mode to request interrupts on
// the rising edges of P10.4 (TA3CCP0) and P10.5 (TA3CCP1).  The
// interrupt service routines acknowledge the interrupt and call
// a user function.
// Input: task0 is a pointer to a user function called when P10.4 (TA3CCP0) edge occurs
//              parameter is 16-bit up-counting timer value when P10.4 (TA3CCP0) edge occurred (units of 0.083 usec)
//        task1 is a pointer to a user function called when P10.5 (TA3CCP1) edge occurs
//              parameter is 16-bit up-counting timer value when P10.5 (TA3CCP1) edge occurred (units of 0.083 usec)
// Output: none
// Assumes: low-speed subsystem master clock is 12 MHz
void TimerA3Capture_Init(void(*task0)(uint16_t time), void(*task2)(uint16_t time)){
	// write this as part of lab 16
	CaptureTask0 =  task0;	           // user function
	CaptureTask2 = 	task2;    	       // user function2
	
	// initialize P10.4 in primary mode as input (P10.4 TA3CCP0)
	P10->SEL0 |= 0x10;
    P10->SEL1 &= ~0x10;
    P10->DIR &= ~0x10;
	// initialize P10.5 in primary mode as input (P10.5 TA3CCP1)
    P10->SEL0 |= 0x20;
    P10->SEL1 &= ~0x20;
    P10->DIR &= ~0x20;
	// halt Timer A3
	TIMER_A3->CTL &= ~0x0030;
	

	// clock source to SMCLK, input clock divider /1, TACLR = 0, stop mode, 
	// interrupt disabled, no interrupt pending
	TIMER_A3->CTL = 0x0200;
	
	// capture on rising edgem, capture/compare input on CCI3A, synchronous capture source
	// capture mode, output mode, enable capture/compare interrupt, no interrupt pending
	TIMER_A3->CCTL[0] = 0x4910;
	TIMER_A3->CCTL[1] = 0x4910;
	
	// configure for input clock divider /1
	TIMER_A3->EX0 &= ~0x0007;

	NVIC->IP[3] = (NVIC->IP[3]&0x00FFFFFF)|0x40000000; // TA3_0 priority 2
	NVIC->IP[3] = (NVIC->IP[3]&0xFF00FFFF)|0x00400000; // TA3_N priority 2
	
	NVIC->ISER[0] = 0x0000C000; // enable interrupt 15 and 14 in NVIC
	
	// Continuous mode and reset TAxR
	TIMER_A3->CTL |= 0x00000024;
}

void TA3_0_IRQHandler(void){
	// write this as part of lab 16
	TIMER_A3->CCTL[0] &= ~0x0001;    		// acknowledge capture/compare interrupt 0
	(*CaptureTask0)(TIMER_A3->CCR[0]);// execute user task
}

void TA3_N_IRQHandler(void){
	TIMER_A3->CCTL[1] &= ~0x0001;;  // acknowledge capture/compare interrupt 1
	(*CaptureTask2)(TIMER_A3->CCR[1]);// execute user task
}

